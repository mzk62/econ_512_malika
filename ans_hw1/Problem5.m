A = normrnd(10, 5, 5, 3)
%Method 1

B = A>10

%Method 2

A(A>10) = 1
A(A~=1) = NaN
A(isnan(A)) = 0;
C = ~A