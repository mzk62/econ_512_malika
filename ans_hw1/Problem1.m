clc; clear;

X = [1, 1.5, 3, 4, 5, 7, 9, 10];

Y1 = -2 + 0.5*X;

 
Y2 = -2 + 0.5*(X.^2);

figure
plot (X,Y1,'r', X,Y2, 'b')
title ('Graphs of Y1 and Y2')
xlabel('X')
ylabel('Y1 and Y2')
legend('Y1', 'Y2')
