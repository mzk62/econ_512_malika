A = [2 4 6; 1 7 5; 3 12 4];
B = [-2 3 10]';
C = A'*B
D = inv(A'*A)*B

% Method 1: Diagonal
% E is a scalar
E = diag(B)*A

% Method 2: Repmat
tmat = repmat(B,1,3)
E2 = tmat.*A

% Method 3: Indexing
A = A(:);
B = B(:, ones(1,3));
B = B(:);
E4 = A.*B;
E4 = reshape(E4,3,3)
E4

A = [2 4 6; 1 7 5; 3 12 4];
A(2,:)=[]
A(:,3)=[]
F = A
A = [2 4 6; 1 7 5; 3 12 4];
B = [-2 3 10]';
X = linsolve(A,B)

