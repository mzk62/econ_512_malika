A = [2 4 6; 1 7 5; 3 12 4];
%Method 1

N = 5;                                                  % Number Of Times To Repeat
Ar = repmat(A, 1, N);                                   % Repeat Matrix
Ac = mat2cell(Ar, size(A,1), repmat(size(A,2),1,N));    % Create Cell Array Of Orignal Repeated Matrix
Out = blkdiag(Ac{:})                                    % Desired Result

%Method 2
% check out the kron() command
out = blkdiag(A, A, A, A, A);